# Pack Together - an online collaboration packing list

Especially for families, packing lists are more complex as for single persons. Packing and checking the items is often done by both one's parents - in parallel. And there often is the need to distinguish by person.

This projects provides another packing list app that focusses on collaboration and family features.

## How to start

This repository is the base to start with. It includes run-scripts for all the microservices of the app, that live in separated repositories.

As open-source application, you can run it by your own. Using Docker, it's easy to start. Simply run

    docker-compose up -d

in the root folder of the project. Check-out the `docker-compose.yaml` file for details.

If you are a Node.js developer, you also can run the application, containing of several microservices, by running

    yarn start-dev


## Behind the scenes

There are the following services involved:

### ListService
Back-end service (Node.js) + MongoDB; holds the packing lists including groups, entries etc.

### UserService
Back-end service (Node.js) + Redis; holds user data for authentication and authorization.

### UI (React)
The application provides a user-interface to the user and communicates (only) via the UI-middleware with other microservices. It also uses WebSockets to sync all changes made from collaborating users.

### UI-Middleware
A middleware as gateway for the UI.


## How it works

The backend microservices communicate via [Cote](https://github.com/dashersw/cote).

Changes are propagated via Cote directly to other services. The ListService provides a WebSocket server to sync changes.
